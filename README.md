# patientPathway

## Introduction

This is an R package for standardizing hospital flows studies, and their modeling in the form of Sankey diagrams, in an observational database based on the OMOP Common Data Model.

## Features
- Extracts the necessary data to build the patient path from a database in OMOP Common Data Model format.
- From the different concept id, it finds the associated domain. Then retrieves the data in the drugs, visit details, procedure and Observations tables.
- Includes a function to work only on cohorts of people defined according to the OMOP data model.
- A function is used to clean up the labels.
- It appropriately simplifies the patient journey by merging, for example, passages in identical units which follow one another.
- Provides a sankey diagram of the study and associated traces.

## Screenshots

(add a sankey diagram)

## System requirements
Requires R. Some of the packages used by patientPathway require Java.

## Development
PatientPathway is being developed in R Studio.

### Development status
PatientPathway is under development.