SELECT
  person_id,
  visit_occurrence_id,
  visit_detail_id,
  condition_concept_id as concept_id,
	condition_start_date as step_start_date,
	condition_end_date as step_end_date,
	'cond' as source
FROM @cdmDatabaseSchema.condition_occurrence
WHERE condition_concept_id IN (@a)
;
