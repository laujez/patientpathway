SELECT
  person_id,
  visit_occurrence_id,
  visit_detail_id,
  observation_concept_id as concept_id,
	observation_date as step_start_date,
	observation_date as step_end_date,
	'obs' as source
FROM @cdmDatabaseSchema.observation
WHERE observation_concept_id IN (@a)
;
