SELECT
  person_id,
  visit_occurrence_id,
  visit_detail_id,
  procedure_concept_id as concept_id,
	procedure_date as step_start_date,
	procedure_date as step_end_date,
	'proc' as source
FROM @cdmDatabaseSchema.procedure_occurrence
WHERE procedure_concept_id IN (@a)
;
