SELECT
  person_id,
  visit_occurrence_id,
  visit_detail_id,
  visit_detail_concept_id as concept_id,
	visit_detail_start_date as step_start_date,
	visit_detail_end_date as step_end_date,
	'vd' as source
FROM @cdmDatabaseSchema.visit_detail
WHERE visit_detail_concept_id IN (@a)
;
