SELECT
  person_id,
  visit_occurrence_id,
  0 as visit_detail_id,
  visit_concept_id as concept_id,
	visit_start_date as step_start_date,
	visit_end_date as step_end_date,
	'vo' as source
FROM @cdmDatabaseSchema.visit_occurrence
WHERE visit_concept_id IN (@a)
;
