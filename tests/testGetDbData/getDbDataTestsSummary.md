# Summary of the tests used on the function getDbData:

## Test 1:
This test data set is made up of 10 patients and 13 stays in total.  It makes it possible **to check whether the simplification of the patient journey during a stay is being done correctly**. 
In particular, to ensure that patients with a stay consisting of a single RUM or several identical RUMs do not disappear.

## Test 2: 
This test dataset includes 10 patients, 13 stays and 13 procedures in total. It makes it possible **to check whether the simplification of the patient journey during a stay is being done correctly**. 
This time we include a procedure for stays.

## Test 3:
This test data set includes 10 patients and 13 stays. For each stay there are 2 exit modes: Home and Death. The interest here is to integrate within a certain stay:
- several RUMs taking place **on the same date**
- an output mode **of the same date** as one or more RUMs

## Test 4:
This test data set includes 15 patients with stays including surgery of interest: total gastrotectomy (4131021), and other stays characterized as readmissions following the procedure. 

We still find concepts that have **identical dates**.

## Test 5: 
This test data set includes 15 patients with stays including surgery of interest: total gastrotectomy (4131021), other stays characterized as readmissions and the final discharge method.

We still find concepts that have **identical dates**.