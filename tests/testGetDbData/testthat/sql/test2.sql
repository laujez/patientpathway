CREATE SCHEMA test_2 AUTHORIZATION postgres;

-- table person--
CREATE TABLE test_2.person
(
  person_id						BIGINT	  	NOT NULL , 
  gender_concept_id				INTEGER	  	NOT NULL ,
  year_of_birth					INTEGER	  	NOT NULL ,
  month_of_birth				INTEGER	  	NULL,
  day_of_birth					INTEGER	  	NULL,
  birth_datetime				TIMESTAMP	NULL,
  death_datetime				TIMESTAMP	NULL,
  race_concept_id				INTEGER		NOT NULL,
  ethnicity_concept_id			INTEGER	  	NOT NULL,
  location_id					BIGINT		NULL,
  provider_id					BIGINT		NULL,
  care_site_id					BIGINT		NULL,
  person_source_value			VARCHAR(50)	NULL,
  gender_source_value			VARCHAR(50) NULL,
  gender_source_concept_id	  	INTEGER		NOT NULL,
  race_source_value				VARCHAR(50) NULL,
  race_source_concept_id		INTEGER		NOT NULL,
  ethnicity_source_value		VARCHAR(50) NULL,
  ethnicity_source_concept_id	INTEGER		NOT NULL
)
;

--table visit_occurence--
CREATE TABLE test_2.visit_occurrence
(
  visit_occurrence_id			BIGINT			NOT NULL ,
  person_id						BIGINT			NOT NULL ,
  visit_concept_id				INTEGER			NOT NULL ,
  visit_start_date				DATE			NULL ,
  visit_start_datetime			TIMESTAMP		NOT NULL ,
  visit_end_date				DATE			NULL ,
  visit_end_datetime			TIMESTAMP		NOT NULL ,
  visit_type_concept_id			INTEGER			NOT NULL ,
  provider_id					BIGINT			NULL,
  care_site_id					BIGINT			NULL,
  visit_source_value			VARCHAR(50)		NULL,
  visit_source_concept_id		INTEGER			NOT NULL ,
  admitted_from_concept_id      INTEGER     	NOT NULL ,   
  admitted_from_source_value    VARCHAR(50) 	NULL ,
  discharge_to_source_value		VARCHAR(50)		NULL ,
  discharge_to_concept_id		INTEGER   		NOT NULL ,
  preceding_visit_occurrence_id	BIGINT 			NULL
)
;

--table visit_detail--
CREATE TABLE test_2.visit_detail
(
  visit_detail_id                    BIGINT      NOT NULL ,
  person_id                          BIGINT      NOT NULL ,
  visit_detail_concept_id            INTEGER     NOT NULL ,
  visit_detail_start_date            DATE        NULL ,
  visit_detail_start_datetime        TIMESTAMP   NOT NULL ,
  visit_detail_end_date              DATE        NULL ,
  visit_detail_end_datetime          TIMESTAMP   NOT NULL ,
  visit_detail_type_concept_id       INTEGER     NOT NULL ,
  provider_id                        BIGINT      NULL ,
  care_site_id                       BIGINT      NULL ,
  discharge_to_concept_id            INTEGER     NOT NULL ,
  admitted_from_concept_id           INTEGER     NOT NULL , 
  admitted_from_source_value         VARCHAR(50) NULL ,
  visit_detail_source_value          VARCHAR(50) NULL ,
  visit_detail_source_concept_id     INTEGER     NOT NULL ,
  discharge_to_source_value          VARCHAR(50) NULL ,
  preceding_visit_detail_id          BIGINT      NULL ,
  visit_detail_parent_id             BIGINT      NULL ,
  visit_occurrence_id                BIGINT      NOT NULL
)
;

CREATE TABLE test_2.concept (
  concept_id			INTEGER			NOT NULL ,
  concept_name			VARCHAR(255)	NOT NULL ,
  domain_id				VARCHAR(20)		NOT NULL ,
  vocabulary_id			VARCHAR(20)		NOT NULL ,
  concept_class_id		VARCHAR(20)		NOT NULL ,
  standard_concept		VARCHAR(1)		NULL ,
  concept_code			VARCHAR(50)		NOT NULL ,
  valid_start_date		DATE			NOT NULL ,
  valid_end_date		DATE			NOT NULL ,
  invalid_reason		VARCHAR(1)		NULL
)
;

insert into test_2.concept
values
(9201,'Inpatient Visit','Visit','Visit','Visit','','IP','1970-01-01','2099-12-31',''),
(32037,'Intensive Care','Visit','Visit','Visit','','OMOP4822460','1970-01-01','2099-12-31',''),
(4131021,'Total gastrectomy','Procedure','Visit','Procedure','','26452005','2002-01-31','2099-12-31','')
;

--10 person--
INSERT INTO test_2.person 
values
(1, 0, 1980, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(2, 1, 1987, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(3, 0, 1990, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(4, 1, 1990, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(5, 0, 2000, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(6, 1, 1991, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(7, 0, 1970, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(8, 1, 1960, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(9, 1, 1949, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(10, 0, 1998, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0);

--13 visit_occurence--
INSERT into test_2.visit_occurrence
values
--patient 1--
(1, 1, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(10, 1, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-30',  '2020-01-30 00:00:20', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 2--
(2, 2, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-07',  '2020-01-07 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 3--
(3, 3, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(12, 3, 9201, '2020-02-03', '2020-02-03 00:00:00', '2020-02-18',  '2020-02-18 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 4--
(4, 4, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 5--
(5, 5, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-02-02',  '2020-02-02 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(13, 5, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 6--
(6, 6, 9201, '2020-02-01', '2020-02-01 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 7--
(7, 7, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-30',  '2020-01-30 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 8--
(8, 8, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 9--
(9, 9, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 10--
(11, 10, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-15',  '2020-01-15 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0);

-- visit_details--
INSERT Into test_2.visit_detail
values
--patient 1-- 
--s�jour 1--
(1, 1, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 1),
--s�jour 2--
(22, 1, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
(23, 1, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-13',  '2020-01-13 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
(24, 1, 32037, '2020-01-13', '2020-01-13 00:00:00', '2020-01-18',  '2020-01-18 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
(25, 1, 9201, '2020-01-18', '2020-01-18 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
--patient 2-- 
--s�jour 1--
(2, 2, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-03',  '2020-01-03 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 2),
(3, 2, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-07',  '2020-01-07 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 2),
--patient 3-- 
--s�jour 1--
(4, 3, 32037, '2020-01-03', '2020-01-03 00:00:00', '2020-01-05',  '2020-01-05 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
(5, 3, 9201, '2020-01-05', '2020-01-05 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
--s�jour 2--
(30, 3, 9201, '2020-02-03', '2020-02-03 00:00:00', '2020-02-05',  '2020-02-05 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
(31, 3, 32037, '2020-02-05', '2020-02-05 00:00:00', '2020-02-10',  '2020-02-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
(32, 3, 32037, '2020-02-10', '2020-02-10 00:00:00', '2020-02-15',  '2020-02-15 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
(33, 3, 9201, '2020-02-15', '2020-02-15 00:00:00', '2020-02-18',  '2020-02-18 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
--patient 4-- 
--s�jour 1--
(6, 4, 32037, '2020-01-07', '2020-01-07 00:00:00', '2020-01-11',  '2020-01-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 4),
(7, 4, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 4),
(8, 4, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 4),
--patient 5-- 
--s�jour 1--
(9, 5, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 5),
(10, 5, 32037, '2020-01-12', '2020-01-12 00:00:00', '2020-01-30',  '2020-01-30 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 5),
(11, 5, 9201, '2020-01-30', '2020-01-30 00:00:00', '2020-02-02',  '2020-02-02 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 5),
--s�jour 2--
(34, 5, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-11',  '2020-02-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 13),
(35, 5, 9201, '2020-02-11', '2020-02-09 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 13),
--patient 6-- 
--s�jour 1--
(12, 6, 32037, '2020-02-01', '2020-02-01 00:00:00', '2020-02-08',  '2020-02-08 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 6),
(13, 6, 32037, '2020-02-08', '2020-02-01 00:00:00', '2020-02-10',  '2020-02-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 6),
(14, 6, 9201, '2020-02-10', '2020-02-10 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 6),
--patient 7-- 
--s�jour 1--
(15, 7, 32037, '2020-01-02', '2020-01-02 00:00:00', '2020-01-18',  '2020-01-18 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(16, 7, 9201, '2020-01-18', '2020-01-18 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(17, 7, 32037, '2020-01-20', '2020-01-20 00:00:00', '2020-01-28',  '2020-01-28 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(18, 7, 9201, '2020-01-28', '2020-01-28 00:00:00', '2020-01-30',  '2020-01-30 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
--patient 8-- 
--s�jour 1--
(19, 8, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-11',  '2020-01-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 8),
(20, 9, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 9),
--patient 9-- 
--s�jour 1--
(21, 9, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 9),
--patient 10-- 
--s�jour 1--
(26, 10, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-03',  '2020-01-03 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
(27, 10, 32037, '2020-01-03', '2020-01-03 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
(28, 10, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-11',  '2020-01-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
(29, 10, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-15',  '2020-01-15 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11);


--table procedure_occurence--

CREATE TABLE test_2.procedure_occurrence
(
  procedure_occurrence_id		BIGINT			NOT NULL ,
  person_id						BIGINT			NOT NULL ,
  procedure_concept_id			INTEGER			NOT NULL ,
  procedure_date				DATE			NULL ,
  procedure_datetime			TIMESTAMP		NOT NULL ,
  procedure_type_concept_id		INTEGER			NOT NULL ,
  modifier_concept_id			INTEGER			NOT NULL ,
  quantity						INTEGER			NULL ,
  provider_id					BIGINT			NULL ,
  visit_occurrence_id			BIGINT			NULL ,
  visit_detail_id             	BIGINT      	NULL ,
  procedure_source_value		VARCHAR(50)		NULL ,
  procedure_source_concept_id	INTEGER			NOT NULL ,
  modifier_source_value		    VARCHAR(50)		NULL 
)
;

INSERT Into test_2.procedure_occurrence
values
--patient1 
--sejour1--
(1,1,4131021,'2020-01-01', '2020-01-01 00:00:00',0,0,0,0,1,1,'',0,''),
--s�jour2--
(2,1,4131021,'2020-01-20','2020-01-20 00:00:00',0,0,0,0,10,25,'',0,''),
--patient2--
--s�jour1--
(3,2,4131021,'2020-01-03','2020-01-03 00:00:00',0,0,0,0,2,3,'',0,''),
--patient3--
--s�jour1--
(4,3,4131021,'2020-01-04','2020-01-04 00:00:00',0,0,0,0,3,4,'',0,''),
--s�jour2--
(5,3,4131021,'2020-02-11','2020-02-11 00:00:00',0,0,0,0,12,32,'',0,''),
--patient4--
--s�jour1--
(6,4,4131021,'2020-01-09','2020-01-09 00:00:00',0,0,0,0,4,6,'',0,''),
--patient5--
--s�jour1--
(7,5,4131021,'2020-01-10','2020-01-10 00:00:00',0,0,0,0,5,9,'',0,''),
--s�jour2--
(8,5,4131021,'2020-02-10','2020-02-10 00:00:00',0,0,0,0,13,34,'',0,''),
--patient6--
--s�jour1--
(9,6,4131021,'2020-02-10','2020-02-10 00:00:00',0,0,0,0,6,14,'',0,''),
--patient7--
--s�jour1--
(10,7,4131021,'2020-01-21','2020-01-21 00:00:00',0,0,0,0,7,17,'',0,''),
--patient8--
--s�jour1--
(11,8,4131021,'2020-01-08','2020-01-08 00:00:00',0,0,0,0,8,19,'',0,''),
--patient9--
--s�jour1--
(12,9,4131021,'2020-01-12','2020-01-12 00:00:00',0,0,0,0,9,21,'',0,''),
--patient10--
--s�jour1--
(13,10,4131021,'2020-01-01','2020-01-01 00:00:00',0,0,0,0,11,26,'',0,'')
;
--4131021 = total gastrectomy


		
