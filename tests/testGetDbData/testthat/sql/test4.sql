CREATE SCHEMA test_4 AUTHORIZATION postgres;

--test sur les r�hospitalsiations avec proc�dure : Total gastrectomy 

-- table person--
CREATE TABLE test_4.person
(
  person_id						BIGINT	  	NOT NULL , 
  gender_concept_id				INTEGER	  	NOT NULL ,
  year_of_birth					INTEGER	  	NOT NULL ,
  month_of_birth				INTEGER	  	NULL,
  day_of_birth					INTEGER	  	NULL,
  birth_datetime				TIMESTAMP	NULL,
  death_datetime				TIMESTAMP	NULL,
  race_concept_id				INTEGER		NOT NULL,
  ethnicity_concept_id			INTEGER	  	NOT NULL,
  location_id					BIGINT		NULL,
  provider_id					BIGINT		NULL,
  care_site_id					BIGINT		NULL,
  person_source_value			VARCHAR(50)	NULL,
  gender_source_value			VARCHAR(50) NULL,
  gender_source_concept_id	  	INTEGER		NOT NULL,
  race_source_value				VARCHAR(50) NULL,
  race_source_concept_id		INTEGER		NOT NULL,
  ethnicity_source_value		VARCHAR(50) NULL,
  ethnicity_source_concept_id	INTEGER		NOT NULL
)
;

--table visit_occurence--
CREATE TABLE test_4.visit_occurrence
(
  visit_occurrence_id			BIGINT			NOT NULL ,
  person_id						BIGINT			NOT NULL ,
  visit_concept_id				INTEGER			NOT NULL ,
  visit_start_date				DATE			NULL ,
  visit_start_datetime			TIMESTAMP		NOT NULL ,
  visit_end_date				DATE			NULL ,
  visit_end_datetime			TIMESTAMP		NOT NULL ,
  visit_type_concept_id			INTEGER			NOT NULL ,
  provider_id					BIGINT			NULL,
  care_site_id					BIGINT			NULL,
  visit_source_value			VARCHAR(50)		NULL,
  visit_source_concept_id		INTEGER			NOT NULL ,
  admitted_from_concept_id      INTEGER     	NOT NULL ,   
  admitted_from_source_value    VARCHAR(50) 	NULL ,
  discharge_to_source_value		VARCHAR(50)		NULL ,
  discharge_to_concept_id		INTEGER   		NOT NULL ,
  preceding_visit_occurrence_id	BIGINT 			NULL
)
;

--table visit_detail--
CREATE TABLE test_4.visit_detail
(
  visit_detail_id                    BIGINT      NOT NULL ,
  person_id                          BIGINT      NOT NULL ,
  visit_detail_concept_id            INTEGER     NOT NULL ,
  visit_detail_start_date            DATE        NULL ,
  visit_detail_start_datetime        TIMESTAMP   NOT NULL ,
  visit_detail_end_date              DATE        NULL ,
  visit_detail_end_datetime          TIMESTAMP   NOT NULL ,
  visit_detail_type_concept_id       INTEGER     NOT NULL ,
  provider_id                        BIGINT      NULL ,
  care_site_id                       BIGINT      NULL ,
  discharge_to_concept_id            INTEGER     NOT NULL ,
  admitted_from_concept_id           INTEGER     NOT NULL , 
  admitted_from_source_value         VARCHAR(50) NULL ,
  visit_detail_source_value          VARCHAR(50) NULL ,
  visit_detail_source_concept_id     INTEGER     NOT NULL ,
  discharge_to_source_value          VARCHAR(50) NULL ,
  preceding_visit_detail_id          BIGINT      NULL ,
  visit_detail_parent_id             BIGINT      NULL ,
  visit_occurrence_id                BIGINT      NOT NULL
)
;
CREATE TABLE test_4.concept (
  concept_id			INTEGER			NOT NULL ,
  concept_name			VARCHAR(255)	NOT NULL ,
  domain_id				VARCHAR(20)		NOT NULL ,
  vocabulary_id			VARCHAR(20)		NOT NULL ,
  concept_class_id		VARCHAR(20)		NOT NULL ,
  standard_concept		VARCHAR(1)		NULL ,
  concept_code			VARCHAR(50)		NOT NULL ,
  valid_start_date		DATE			NOT NULL ,
  valid_end_date		DATE			NOT NULL ,
  invalid_reason		VARCHAR(1)		NULL
)
;

insert into test_4.concept
values
(9201,'Inpatient Visit','Visit','Visit','Visit','','IP','1970-01-01','2099-12-31',''),
(32037,'Intensive Care','Visit','Visit','Visit','','OMOP4822460','1970-01-01','2099-12-31',''),
(4131021,'Total gastrectomy','Procedure','Visit','Procedure','','26452005','2002-01-31','2099-12-31','')
;


--15 person--
INSERT INTO test_4.person 
values
(1, 0, 1980, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(2, 1, 1987, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(3, 0, 1990, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(4, 1, 1990, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(5, 0, 2000, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(6, 1, 1991, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(7, 0, 1970, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(8, 1, 1960, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(9, 1, 1949, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(10, 0, 1998, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(11, 0, 2000, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(12, 1, 1991, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(13, 0, 1960, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(14, 1, 1955, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0),
(15, 1, 1949, 0, 0, null, null, 0, 0, 0, 0, 0, '', '', 0, '', 0, '', 0)
;

--32 visit_occurence--
INSERT into test_4.visit_occurrence
values
--patient 1--
(1, 1, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(2, 1, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-22',  '2020-01-22 00:00:20', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(3, 1, 9201, '2020-02-10', '2020-02-10 00:00:00', '2020-02-20',  '2020-02-20 00:00:20', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 2--
(4, 2, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 3--
(5, 3, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-05',  '2020-01-05 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(6, 3, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(7, 3, 9201, '2020-01-17', '2020-01-17 00:00:00', '2020-01-25',  '2020-01-25 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(8, 3, 9201, '2020-02-03', '2020-02-03 00:00:00', '2020-02-15',  '2020-02-15 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 4--
(9, 4, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(10, 4, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 5--
(11, 5, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(12, 5, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 6--
(13, 6, 9201, '2020-02-01', '2020-02-01 00:00:00', '2020-02-10',  '2020-02-10 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(14, 6, 9201, '2020-02-27', '2020-02-27 00:00:00', '2020-02-28',  '2020-02-28 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(15, 6, 9201, '2020-03-07', '2020-03-07 00:00:00', '2020-03-14',  '2020-03-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 7--
(16, 7, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-30',  '2020-01-30 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(17, 7, 9201, '2020-02-07', '2020-02-07 00:00:00', '2020-02-14',  '2020-02-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 8--
(18, 8, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(19, 8, 9201, '2020-01-20', '2020-01-20 00:00:00', '2020-01-21',  '2020-01-21 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(20, 8, 9201, '2020-03-07', '2020-03-07 00:00:00', '2020-03-14',  '2020-03-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 9--
(21, 9, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 10--
(22, 10, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-15',  '2020-01-15 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0,0 ),
--patient 11--
(23, 11, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(24, 11, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-20',  '2020-01-20 00:00:20', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 12--
(25, 12, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-02',  '2020-01-02 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(26, 12, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(27, 12, 9201, '2020-02-07', '2020-02-07 00:00:00', '2020-02-14',  '2020-02-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 13--
(28, 13, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 14--
(29, 14, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
--patient 15--
(30, 15, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0),
(31, 15, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, '', 0, 0, '', '', 0, 0);

-- 75 visit_details--
INSERT Into test_4.visit_detail
values
--patient 1-- 
--s�jour 1--
(1, 1, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 1),
--s�jour 2--
(2, 1, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-20',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 2),
(3, 1, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-13',  '2020-01-13 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 2),
(4, 1, 32037, '2020-01-13', '2020-01-13 00:00:00', '2020-01-22',  '2020-01-22 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 2),
--s�jour 3--
(5, 1, 9201, '2020-02-10', '2020-02-10 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
(6, 1, 9201, '2020-02-12', '2020-02-12 00:00:00', '2020-02-13',  '2020-02-13 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
(7, 1, 32037, '2020-02-13', '2020-02-13 00:00:00', '2020-02-18',  '2020-02-18 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
(8, 1, 9201, '2020-02-18', '2020-02-18 00:00:00', '2020-02-20',  '2020-02-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 3),
--patient 2-- 
--s�jour 1--
(9, 2, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-02',  '2020-01-02 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 4),
(10, 2, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 4),
--patient 3-- 
--s�jour 1--
(11, 3, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-05',  '2020-01-05 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 5),
--s�jour 2--
(12, 3, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 6),
(13, 3, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 6),
--s�jour 3--
(14, 3, 9201, '2020-01-17', '2020-01-17 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(15, 3, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(16, 3, 32037, '2020-01-20', '2020-01-20 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
(17, 3, 9201, '2020-01-20', '2020-01-20 00:00:00', '2020-01-25',  '2020-01-25 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 7),
--s�jour 4--
(18, 3, 9201, '2020-02-03', '2020-02-03 00:00:00', '2020-02-04',  '2020-02-04 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 8),
(19, 3, 9201, '2020-02-04', '2020-02-04 00:00:00', '2020-02-06',  '2020-02-06 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 8),
(20, 3, 32037, '2020-02-06', '2020-02-06 00:00:00', '2020-02-09',  '2020-02-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 8),
(21, 3, 9201, '2020-02-09', '2020-02-09 00:00:00', '2020-02-15',  '2020-02-15 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 8),
--patient 4-- 
--s�jour 1--
(22, 4, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 9),
(23, 4, 32037, '2020-01-09', '2020-01-09 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 9),
(24, 4, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 9),
--s�jour 2--
(25, 4, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
(26, 4, 9201, '2020-01-19', '2020-01-19 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 10),
--patient 5-- 
--s�jour 1--
(27, 5, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
(28, 5, 32037, '2020-01-10', '2020-01-10 00:00:00', '2020-01-17',  '2020-01-17 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
(29, 5, 9201, '2020-01-17', '2020-01-17 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 11),
--s�jour 2--
(30, 5, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-11',  '2020-02-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
(31, 5, 9201, '2020-02-11', '2020-02-09 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 12),
--patient 6-- 
--s�jour 1--
(32, 6, 9201, '2020-02-01', '2020-02-01 00:00:00', '2020-02-10',  '2020-02-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 13),
--s�jour 2--
(33, 6, 9201, '2020-02-27', '2020-02-27 00:00:00', '2020-02-28',  '2020-02-28 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 14),
--s�jour 3--
(34, 6, 9201, '2020-03-07', '2020-03-07 00:00:00', '2020-03-09',  '2020-03-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 15),
(35, 6, 9201, '2020-03-09', '2020-03-09 00:00:00', '2020-03-11',  '2020-03-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 15),
(36, 6, 32037, '2020-03-11', '2020-03-11 00:00:00', '2020-03-12',  '2020-03-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 15),
(37, 6, 9201, '2020-03-12', '2020-03-12 00:00:00', '2020-03-14',  '2020-03-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 15),
--patient 7-- 
--s�jour 1--
(38, 7, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-05',  '2020-01-05 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 16),
(39, 7, 32037, '2020-01-05', '2020-01-05 00:00:00', '2020-01-15',  '2020-01-15 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 16),
(40, 7, 9201, '2020-01-15', '2020-01-15 00:00:00', '2020-01-30',  '2020-01-30 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 16),
--s�jour 2--
(41, 7, 9201, '2020-02-07', '2020-02-07 00:00:00', '2020-02-11',  '2020-02-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 17),
(42, 7, 9201, '2020-02-11', '2020-02-09 00:00:00', '2020-02-14',  '2020-02-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 17),
--patient 8-- 
--s�jour 1--
(43, 8, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 18),
--s�jour 2--
(44, 8, 9201, '2020-01-20', '2020-01-20 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 19),
(45, 8, 9201, '2020-01-20', '2020-01-20 00:00:00', '2020-01-21',  '2020-01-21 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 19),
--s�jour 3--
(46, 8, 9201, '2020-03-07', '2020-03-07 00:00:00', '2020-03-09',  '2020-03-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 20),
(47, 8, 9201, '2020-03-09', '2020-03-09 00:00:00', '2020-03-10',  '2020-03-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 20),
(48, 8, 32037, '2020-03-10', '2020-03-10 00:00:00', '2020-03-13',  '2020-03-13 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 20),
(49, 8, 9201, '2020-03-13', '2020-03-13 00:00:00', '2020-03-14',  '2020-03-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 20),
--patient 9-- 
--s�jour 1--
(50, 9, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 21),
--patient 10-- 
--s�jour 1--
(51, 10, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-03',  '2020-01-03 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 22),
(52, 10, 32037, '2020-01-03', '2020-01-03 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0,22),
(53, 10, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-11',  '2020-01-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 22),
(54, 10, 9201, '2020-01-11', '2020-01-11 00:00:00', '2020-01-15',  '2020-01-15 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 22),
--patient 11-- 
--s�jour 1--
(55, 11, 9201, '2020-01-01', '2020-01-01 00:00:00', '2020-01-02',  '2020-01-02 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 23),
(56, 11, 32037, '2020-01-02', '2020-01-02 00:00:00', '2020-01-06',  '2020-01-06 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 23),
(57, 11, 9201, '2020-01-06', '2020-01-06 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 23),
--s�jour 2--
(58, 11, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-11',  '2020-01-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 24),
(59, 11, 9201, '2020-01-11', '2020-01-09 00:00:00', '2020-01-20',  '2020-01-20 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 24),
--patient 12-- 
--s�jour 1--
(60, 12, 9201, '2020-01-02', '2020-01-02 00:00:00', '2020-01-02',  '2020-01-02 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 25),
--s�jour 2--
(61, 12, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-08',  '2020-01-08 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 26),
(62, 12, 9201, '2020-01-08', '2020-01-08 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 26),
(63, 12, 32037, '2020-01-10', '2020-01-10 00:00:00', '2020-01-12',  '2020-01-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 26),
(64, 12, 9201, '2020-01-12', '2020-01-12 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 26),
--s�jour 3--
(65, 12, 9201, '2020-02-07', '2020-02-07 00:00:00', '2020-02-09',  '2020-02-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 27),
(66, 12, 9201, '2020-02-09', '2020-02-09 00:00:00', '2020-02-10',  '2020-02-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 27),
(67, 12, 32037, '2020-02-10', '2020-02-10 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 27),
(68, 12, 9201, '2020-02-12', '2020-02-12 00:00:00', '2020-02-14',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 27),
--patient 13-- 
--s�jour 1--
(69, 13, 9201, '2020-01-03', '2020-01-03 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 28),
--patient 14-- 
--s�jour 1--
(70, 14, 9201, '2020-01-07', '2020-01-07 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 29),
(71, 14, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-14',  '2020-01-14 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 29),
--patient 15-- 
--s�jour 1--
(72, 15, 9201, '2020-01-09', '2020-01-09 00:00:00', '2020-01-09',  '2020-01-09 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 30),
(73, 15, 32037, '2020-01-09', '2020-01-09 00:00:00', '2020-01-10',  '2020-01-10 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 30),
(74, 15, 9201, '2020-01-10', '2020-01-10 00:00:00', '2020-01-19',  '2020-01-19 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 30),
--s�jour 2--
(75, 15, 9201, '2020-02-08', '2020-02-08 00:00:00', '2020-02-11',  '2020-02-11 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 31),
(76, 15, 9201, '2020-02-11', '2020-02-09 00:00:00', '2020-02-12',  '2020-02-12 00:00:00', 0, 0, 0, 0, 0, '', '', 0, '', 0, 0, 31)
;


--13 proc�dures--
CREATE TABLE test_4.procedure_occurrence
(
  procedure_occurrence_id		BIGINT			NOT NULL ,
  person_id						BIGINT			NOT NULL ,
  procedure_concept_id			INTEGER			NOT NULL ,
  procedure_date				DATE			NULL ,
  procedure_datetime			TIMESTAMP		NOT NULL ,
  procedure_type_concept_id		INTEGER			NOT NULL ,
  modifier_concept_id			INTEGER			NOT NULL ,
  quantity						INTEGER			NULL ,
  provider_id					BIGINT			NULL ,
  visit_occurrence_id			BIGINT			NULL ,
  visit_detail_id             	BIGINT      	NULL ,
  procedure_source_value		VARCHAR(50)		NULL ,
  procedure_source_concept_id	INTEGER			NOT NULL ,
  modifier_source_value		    VARCHAR(50)		NULL 
)
;

INSERT Into test_4.procedure_occurrence
values
--patient1 
--sejour1--
(1,1,4131021,'2020-01-01', '2020-01-01 00:00:00',0,0,0,0,1,1,'',0,''),
--patient2--
--s�jour1--
(2,2,4131021,'2020-01-02','2020-01-02 00:00:00',0,0,0,0,4,9,'',0,''),
--patient3--
--s�jour1--
(3,3,4131021,'2020-01-03','2020-01-03 00:00:00',0,0,0,0,5,11,'',0,''),
--patient4--
--s�jour1--
(4,4,4131021,'2020-01-09','2020-01-09 00:00:00',0,0,0,0,9,23,'',0,''),
--patient5--
--s�jour1--
(5,5,4131021,'2020-01-09','2020-01-09 00:00:00',0,0,0,0,11,27,'',0,''),
--patient6--
--s�jour1--
(6,6,4131021,'2020-02-10','2020-02-10 00:00:00',0,0,0,0,13,32,'',0,''),
--patient7--
--s�jour1--
(7,7,4131021,'2020-01-03','2020-01-03 00:00:00',0,0,0,0,16,38,'',0,''),
--patient8--
--s�jour1--
(8,8,4131021,'2020-01-11','2020-01-11 00:00:00',0,0,0,0,18,43,'',0,''),
--patient9--
--s�jour1--
(9,9,4131021,'2020-01-12','2020-01-12 00:00:00',0,0,0,0,21,50,'',0,''),
--patient10--
--s�jour1--
(10,10,4131021,'2020-01-01','2020-01-01 00:00:00',0,0,0,0,22,51,'',0,''),
--patient11--
--s�jour1--
(11,11,4131021,'2020-01-02','2020-01-02 00:00:00',0,0,0,0,23,55,'',0,''),
--patient12--
--s�jour1--
(12,12,4131021,'2020-01-02','2020-01-02 00:00:00',0,0,0,0,25,60,'',0,''),
--patient13--
--s�jour1--
(13,13,4131021,'2020-01-03','2020-01-03 00:00:00',0,0,0,0,28,69,'',0,''),
--patient14--
--s�jour1--
(14,14,4131021,'2020-01-08','2020-01-08 00:00:00',0,0,0,0,29,70,'',0,''),
--patient15--
--s�jour1--
(15,15,4131021,'2020-01-09','2020-01-09 00:00:00',0,0,0,0,31,75,'',0,'')
;
--4131021 = total gastrectomy
