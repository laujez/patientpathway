CREATE SCHEMA test_8 AUTHORIZATION postgres;
--r�cup�rer les descendants

CREATE TABLE test_8.concept (
  concept_id			INTEGER			NOT NULL ,
  concept_name			VARCHAR(255)	NOT NULL ,
  domain_id				VARCHAR(20)		NOT NULL ,
  vocabulary_id			VARCHAR(20)		NOT NULL ,
  concept_class_id		VARCHAR(20)		NOT NULL ,
  standard_concept		VARCHAR(1)		NULL ,
  concept_code			VARCHAR(50)		NOT NULL ,
  valid_start_date		DATE			NOT NULL ,
  valid_end_date		DATE			NOT NULL ,
  invalid_reason		VARCHAR(1)		NULL
)
;

insert into test_8.concept
values
(4094240,'Open heart surgery','Procedure','SNOMED','Procedure','','2598006','2002-01-31','2099-12-31',''),
(4020086,'Open cardiac valvotomy','Procedure','SNOMED','Procedure','','174966008','2002-01-31','2099-12-31',''),
(4020520,'Open aortic valvotomy','Procedure','SNOMED','Procedure','','174968009','2002-01-31','2099-12-31',''),
(4336904,'Open left ventricular biopsy','Procedure','SNOMED','Procedure','','233077002','2002-01-31','2099-12-31',''),
(4339966,'Open atrial fenestration','Procedure','SNOMED','Procedure','','233041009','2002-01-31','2099-12-31',''),
(4275564,'Operation on heart','Procedure','SNOMED','Procedure','','64915003','2002-01-31','2099-12-31',''),
(4299725,'Operation on mediastinum','Procedure','SNOMED','Procedure','','386765006','2003-01-31','2099-12-31','');


CREATE TABLE test_8.concept_ancestor (
  ancestor_concept_id		INTEGER		NOT NULL,
  descendant_concept_id		INTEGER		NOT NULL,
  min_levels_of_separation	INTEGER		NOT NULL,
  max_levels_of_separation	INTEGER		NOT NULL
)
;

insert into test_8.concept_ancestor
values
(4094240,4094240,0,0),
(4094240,4020086,1,1),
(4094240,4020520,2,2),
(4094240,4336904,1,1),
(4094240,4339966,1,1),
(4275564,4094240,1,1),
(4299725,4094240,2,2);
